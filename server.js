'use strict';

var express = require('express'),
    exphbs = require('express-handlebars'),
	Promise = require('bluebird'),
	_posts = require('./models/posts'),
	_comments = require('./models/comments');

var app = express();

app.engine('handlebars', exphbs.create({defaultLayout: 'main'}).engine);
app.set('view engine', 'handlebars');

app.get('/', function (req, res) {
	_posts.all()
	.then(function(allPosts) {
		res.render('posts', {
			title: 'Posts',
			posts: allPosts
		});
	});
});

app.get('/posts/:id', function (req, res) {
	var postId = req.params.id;
	Promise.props({
		post: _posts.one(postId),
		comments: _comments.ofPost(postId)
	})
	.then(function(data) {
		res.render('singlePost', {
			title: 'singlePost',
			post: data.post,
			comments: data.comments,
		});
	});
});

app.listen(3000, function () {
    console.log('LitBlog server listening on: 3000');
});
