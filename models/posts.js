var request = require('request-promise');

module.exports.all = function () {
	return request('https://jsonplaceholder.typicode.com/posts')
	.then(JSON.parse);
}
module.exports.one = function (id) {
	return request('https://jsonplaceholder.typicode.com/posts/'+id)
	.then(JSON.parse);
}
